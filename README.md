**Links & Authors** <details><summary>**Click to expand**</summary>
<br> Team: Red hat 
<br>Student name: Yeonjin Kim 
<br>Student number: s4622650
<br>Student name: Mengyi Huang (Emily)
<br>Student number: s4618331
<br>Student name: Gabriela Nicole Nathania 
<br>Student number: s4553252
<br>Student name: Prudence Querida Pashahumairah
<br>Student number: s4544562
<br><br>**Links**
<br>Prototype related links:
<br>•	Prototype hyperlink: https://youtu.be/tewPmmdwftM
<br>o	You can see how our two prototypes work in the video.
<br>•	GitHub repository page: https://gitlab.com/prudencequerida/redhat 
<br>o	Where you can find all source codes of our two prototypes.
<br><br>
Other GitHub links:
<br>•	GitHub Wiki page:  https://gitlab.com/prudencequerida/redhat/-/wikis/About
<br>o	On the right side menu, you can find wiki pages, including About, Appendix, Background & Limitation, Design Process, Instructions and Summary pages.
<br>•	GitHub Appendix Page: https://gitlab.com/prudencequerida/redhat/-/wikis/Appendix
<br>o	Where you can find our poster, kickstart video, all meeting notes, each member’s contribution, all interview transcripts, and team contract.
<br><br>
Promotional materials:
<br>•	Kickstart Video Link: https://app.animaker.com/video/JW92O7Y06D4YEIIT
<br>•	Poster Link: https://miro.com/app/board/o9J_lqrpMvI=/?moveToWidget=3074457365891160128&cot=14
<br><br>
Other links:
<br>•	Team miro link: https://miro.com/app/board/o9J_lya4J_k=/
<br>o	You can find how we manage tasks during the semester.
<br>•	2021 Social & Mobile computing exhibit miro link: https://miro.com/app/board/o9J_lqrpMvI=/?moveToWidget=3074457365610182037&cot=14

</details>
<hr>

Below are the steps to use the first prototype (Unity + Arduino):
(Please make sure you download the Arduino and Unity in your laptop already)
- Connect Arduino Lilypad with your laptop.
- Open the folder “Prototype 1- UnitynArdurino”, then open the “arduinoCodes”, open the “buttonsDown” folder, double click to open “buttonsDown.ino” file.
- In Arduino software, go to the top menu “Tools”, select “Adafruit Circuit Playground” as Board Name. Then select the right Port. For my iMac, the Port is “/dev/cu.usbmodemHIDPC1(Adafruit Circuit Playground)”.
- Click “verify” and “upload” After “upload done”, the Lilypad is set up.
- Go back to the folder “Prototype 1- UnitynArdurino”, then open the “UnityCodes”, then open the “Assets” folder, then open the “Scenes” folder, double click the “greatScene.unity” to launch the unity file.
- After the Unity file is open, find the “SerialCommunications.cs” script file, and change the port name(in line 22) to the port name that was used to connect your Lilypad. 
- Then you can click the “run” button of the Unity file. 
- If you are happy, press the right button on the Lilypad to express your happiness. On the Unity display screen, a happy octopus image together with a voice sound saying “I am happy, baby” will show up.
- If you are sad, press the left button on the Lilypad to express your sadness. On the Unity display screen, a sad octopus image together with a voice sound saying “I am sad, baby” will show up.
- The first prototype is built to mimic the express of feelings from one partner, and receive emotional notification by the other partner.


Below are the steps to use and deploy the second prototype (Mobile Application built by Swift code):
(Please make sure you download the Xcode in your laptop already.)
- Open the folder “Prototype 2- mobileApp”, then double click to open “Emotion Check.xcodproj” file.
- If you have an iPhone, you can connect your iPhone with your laptop. Then, choose your connected iphone and click the “Run” button to deploy this application onto your iphone.
- If you do not have an iphone with you at the moment, you can choose an iphone type before running the project. For e.g. ” iPhone 11 pro”. Then, click the “run” button in the Xcode. Please wait for a couple of minutes. Then you will see an iPhone simulator on your screen. 
- There are three tab buttons at the bottom that you can tab and explore: 
- Home: where you can click “Emotion Received ” to see the emotions that your partner had sent to you.
- Profile:where you can link your account with your partner and edit your profile.
- Settings: where you can do customization about your experience, such as mute the sound, change image, change voice sound etc. 




