﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class showhappyorsadface : MonoBehaviour
{
    public Image emptyImage;
    public Sprite happyFace;
    public Sprite sadFace;
    public Sprite transparentImage;
    GameObject popUpMessage;
    public AudioSource happyVoice;
    public AudioSource sadVoice;

    // Start is called before the first frame update
    void Start()
    {
        popUpMessage = GameObject.Find ("Button");
        //hide the pop up text when the game starts.
        popUpMessage.SetActive(false);

    }

 

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.A)){
            emptyImage.sprite = happyFace;
            StartCoroutine( emotioinPopUp());
            StartCoroutine( messagePopUp());
            happyVoice.Play();
        }
     
        if(Input.GetKey(KeyCode.B)){
            emptyImage.sprite = sadFace;
            StartCoroutine( emotioinPopUp());
            StartCoroutine( messagePopUp());
            sadVoice.Play();
        }

    }

     IEnumerator emotioinPopUp()
    {
        // let the emotion icons show for 3 seconds.
        yield return new WaitForSeconds(3);
        emptyImage.sprite = transparentImage;
    }

    IEnumerator messagePopUp()
    {
        // let the text on the top show for 3 seconds.
        popUpMessage.SetActive(true);
        yield return new WaitForSeconds(3);
        popUpMessage.SetActive(false);
    }


}
