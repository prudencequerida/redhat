﻿// (SerialCommunications.cs)Code snippets from DECO 7230 week 8 tutorial slides.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class SerialCommunications : MonoBehaviour
{
    public string portName;
    SerialPort port;
    public string[] ports;
    


    void Start()
    {

        //gets a list of all the ports attached to the system
        ports = SerialPort.GetPortNames();

        //set the port name to be the newest one (bottom of the list)
        portName = "/dev/cu.usbmodem14401"; //<--- CHANGE THIS TO THE PORT USED IN ARDUINO IF IT IS INCORRECT
        //portName = "COM9";  //<----like this

        //setup the port
        port = new SerialPort (portName, 9600);
       
        //set timeouts to a low number to make communication happen faster
        port.ReadTimeout = 10;
        port.WriteTimeout = 10;
        port.DtrEnable = true;
        port.RtsEnable = true;
       
        //open the port
        port.Open();

    }

    void Update()
    {
     

        if (port.IsOpen) { //check that the port is open before trying to use it.
            try // use try/catch to manage non fatal errors (like serial timeouts)
            {
                string value = port.ReadLine().Trim(); //Read the information and trim the whitespace
                Debug.Log("Received>" + value); //view the data in the debug console.

                
            }
            catch
            {
                //don't do anything if errors happen.
            }

           
        }
    }
}
