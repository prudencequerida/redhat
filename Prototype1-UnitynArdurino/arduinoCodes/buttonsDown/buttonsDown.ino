#include <Adafruit_CircuitPlayground.h>
#include "Keyboard.h"

bool leftButtonPressed;
bool rightButtonPressed;

void setup() {
  Serial.begin(9600);
  CircuitPlayground.begin();
  Keyboard.begin();
}

void loop() {
  leftButtonPressed = CircuitPlayground.leftButton();
  rightButtonPressed = CircuitPlayground.rightButton();
  
  //left button pressed is A
  if (leftButtonPressed) {
    Serial.print("A");
    Keyboard.write('A');
  } 
  //right button pressed is B
  if (rightButtonPressed) {
    Serial.print("B");
    Keyboard.write('B');
  } 
  
  delay(100);
}
